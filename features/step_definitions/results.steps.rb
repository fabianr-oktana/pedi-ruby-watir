require_relative '../pageobjects/results.page.rb' 

Given /^Report total no. of results with no. of results in the current page$/ do
    @results = Results.new(@browser)
    @results.report_totals
end

Given /^Select a random Filter$/ do
    @results.select_random_filter
end

Given /^Verify the number of results is correct$/ do
    expect(@results.filter_count).to eq @results.total_found
end

Given /^Sort by: "([^"]*)"$/ do |criteria| 
    @results.sort_by(criteria)
end

Given /^Verify the sort is correct$/ do
    expect(@results.verify_order).to be true;
end

Given /^Report the star rating of each of the results in the first result page$/ do
    @results.report_stars
end

Given /^Go into the first result from the search results$/ do
    @results.go_to_first_result
end

Given /^Log all critical information of the selected restaurant$/ do
    @results.log_rest_info
end