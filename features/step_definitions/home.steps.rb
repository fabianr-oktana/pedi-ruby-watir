require_relative '../pageobjects/home.page.rb' 

Given /^I go to www.pedidosya.com.uy$/ do
    @home = HomePage.new(@browser)
    @home.navigate
end

Given(/^Set any street$/) do
  @home.set_street
end

Given(/^search for "(.*?)"$/) do |search_param|
  @home.search_food(search_param)
end

Given(/^Click Search button$/) do
  @home.click_search
end

Given(/^Confirm the street$/) do
  @home.confirm_street
end
