require 'watir'
require 'selenium-webdriver'
require 'minitest/test'
# require 'webdrivers'
require 'page-object'

if ENV['HEADLESS']
  require 'headless'
  headless = Headless.new
  headless.start
  at_exit do
    headless.destroy
  end
end

