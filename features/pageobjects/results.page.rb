class Results 
    include PageObject

    element(:total_found, :css => '.found > i')
    elements(:total_page, :css => '.restaurant-wrapper.peyaCard.show')
    text_area(:filter_counter, :css => '.js-filter-channel.old_channel i')
    elements(:filters, :css => '.js-filter-channel.old_channel')
    link(:drop_down, :css => '.dropdown.withDrop')
    elements(:open_rests, :css => '[data-status="OPEN"]')
    elements(:closed_rests, :css => '[data-status="CLOSED"]')
    elements(:open_soon_rests, :css => '[data-status^="SOON"]')
    elements(:rows_rest, :css => '.arrivalName')
    elements(:raiting, :css => '.ranking > i')
    h1(:name_detail)
    elements(:rates, :css => 'li > .new_rating')
    span(:distance, :css => '.distance')
    span(:time, :css => '.deliveryTime')
    span(:cost, :css => '.shippingAmount')
    link(:confirm_closed, :css => "[style*='block'] #lnkConfirmPop")
    button(:info_tab, :css => '[data-link="tab-info"]')
    button(:comments_tab, :css => '[data-link="tab-comments"]')
    elements(:hour_list, :css => '[itemprop="openingHoursSpecification"]')
    elements(:comment_row, :css => '[itemprop="review"]')
    elements(:author, :css => '[itemprop="author"]')
    elements(:comm_rate, :css => '.rating-points')
    elements(:comm_date, :css => '[itemprop="datePublished"]')
    elements(:comm_text, :css => '[itemprop="description"]')

    def report_totals
        puts "The search returned a total of #{total_found} results"
        puts "The page is showing #{total_page_elements.length} results"
    end

    def select_random_filter
        random = rand(filters_elements.length-1)
        $FILTER_COUNT = filters_elements[random].i.text
        filters_elements[random].click
    end

    def filter_count
        return $FILTER_COUNT
    end

    def sort_by (criterion)
        $ORDER_CRITERION = criterion
        drop_down
        @browser.link(text: /#{criterion}/).click
    end

    def verify_order
        case $ORDER_CRITERION
        when "Alfabéticamente"
            if !compare_names(open_rests_elements)
                return false
            end
            if !compare_names(open_soon_rests_elements)
                return false
            end
            if !compare_names(closed_rests_elements)
                return false
            end
            return true
        else
            puts "Criterion not established"
            return false
        end
    end

    def compare_names(rest_status_elements) 
        rests_qty = rest_status_elements.length
        if rests_qty > 1 
            rest_status_elements.each_index { |i| 
                if i < rests_qty - 1
                    rest1 = rest_status_elements[i].a.title
                    rest2 = rest_status_elements[i+1].a.title
                    if rest2<rest1
                        return false;
                    end 
                end
            }
        end
        return true
    end

    def report_stars
        raiting_elements.each_index {|i| 
            puts "Name:    #{rows_rest_elements[i].text}" 
            puts "Rating:  #{raiting_elements[i].text}"
            puts
        }
    end
    
    def go_to_first_result
        rows_rest_elements[0].click
    end

    def log_details
        speed_rate = rates_elements[0].text
        food_rate = rates_elements[1].text
        service_rate = rates_elements[2].text
        puts "Name:         #{name_detail}"
        puts "Speed rate:   #{speed_rate}"
        puts "Food rate:    #{food_rate}"
        puts "Service Rate: #{service_rate}"
        # puts "Distance:     #{distance.split("\n")[0]}"
        puts "Time:         #{time.split("\n")[0]}"
        puts "Cost:         #{cost.split("\n")[0]}"
    end

    def log_open_hours
        info_tab
        puts "\nOpen hours:"
        hour_list_elements.each { |i| puts i.text.split(' ',2)[0].ljust(14) << i.text.split(' ',2)[1]}
    end

    def log_comments
        comments_tab
        comment_row_elements.first(5).each do |row| 
            puts "\n>>>>> Author:  #{row.i(author_elements[0].selector).text}"
            puts ">>>>> Date:    #{row.p(comm_date_elements[0].selector).text}"
            puts ">>>>> Rate:    #{row.i(comm_rate_elements[0].selector).text}"
            puts ">>>>> Comment: #{row.p(comm_text_elements[0].selector).text}\n"
        end
    end

    def log_rest_info
        check_closed
        log_details
        log_open_hours
        log_comments
    end

    def check_closed
        if is_present(confirm_closed_element)
            confirm_closed
            puts "Restaurant is open"
        else
            puts "Restaurant is closed"
        end
    end

    def is_present (elem)
        begin
            @browser.wait_until(timeout: 2){ elem.exists? }
            return true
        rescue Watir::Wait::TimeoutError
            return false
        end
    end
end
