class HomePage
    include PageObject

    button(:search, :id => 'search')
    link(:confirm, :id => 'confirm')
    text_field(:street, :id => 'address')
    text_field(:food, :id => 'optional')
    element(:map, :css => '.gmnoprint > img')

    def navigate
        @browser.goto "www.pedidosya.com.uy" 
    end
    
    def set_street
        self.street = "Dr. Enrique Tarigo 1335"
    end

    def search_food(food_text)
        self.food = food_text
    end

    def click_search
        self.search
    end

    def confirm_street
        @browser.wait_until{ self.map_element.exists? }
        self.confirm
    end

end